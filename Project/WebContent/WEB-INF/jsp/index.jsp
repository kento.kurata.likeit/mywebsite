<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link href="css/original/common.css" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	rel="stylesheet">
<title>カレンダー画面</title>

</head>

<body>
	<header>
		<nav class="navbar">
			<div class="header-logo">
				<div class="btn-group">
					<a class="btn" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false"> <i class="fas fa-bars fa-lg my-big"></i>
					</a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="IndexServlet"><i
							class="fas fa-home my-big"></i>Home</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item"
							href="UserDetailServlet?id=${loginInfo.id}"><i
							class="fas fa-user-circle my-big"></i>User Information</a> <a
							class="dropdown-item"
							href="AddCategoryServlet?id=${loginInfo.id}"><i
							class="fas fa-cog my-big"></i>Add Category</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item"
							href="SerchScheduleServlet?id=${loginInfo.id}"><i
							class="fas fa-search my-big"></i>Search Schedule</a> <a
							class="dropdown-item"
							href="AddScheduleServlet?id=${loginInfo.id}"><i
							class="fas fa-plus my-big"></i>Add Schedule</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="LogoutServlet"><i
							class="fas fa-sign-out-alt my-big"></i>Logout</a>
					</div>
				</div>
				<a href="IndexServlet" class="logo">To Do Schedule</a>
			</div>
			<div class="header-icons">
				<a href="IndexServlet" class="home" data-toggle="tooltip"
					data-placement="left" title="Home"><i
					class="fas fa-home my-big"></i></a> <a
					href="SerchScheduleServlet?id=${loginInfo.id}" class="search"
					data-toggle="tooltip" data-placement="left" title="Search Schedule"><i
					class="fas fa-search my-big"></i></a> <a
					href="AddScheduleServlet?id=${loginInfo.id}" class="new-schedule"
					data-toggle="tooltip" data-placement="left" title="Add Schedule"><i
					class="fas fa-plus my-big"></i></a> <a href="LogoutServlet"
					class="logout" data-toggle="tooltip" data-placement="left"
					title="Logout"><i class="fas fa-sign-out-alt my-big"></i></a>
			</div>
		</nav>
	</header>
	<c:choose>
		<c:when
			test="${(priMonth == null && nextMonth == null)|| ((priMonth == currentMonth && selectYear == currentYear) || (nextMonth == currentMonth && selectYear == currentYear))}">
			<div class="container">
				<div class="row">
					<div class="col"></div>
					<div class="col-10">
						<div class="card card-container">
							<h4>${currentYear}年</h4>
							<h4>
								<c:if test="${currentMonth != 12 && currentMonth!= 1}">
									<a
										href="IndexServlet?selectYear=${currentYear}&priMonth=${currentMonth-1}"
										class="month-tooltip" data-toggle="tooltip"
										data-placement="left" title="Privious Month"><i
										class="fas fa-angle-double-left"></i></a>
									${currentMonth}月
									<a
										href="IndexServlet?selectYear=${currentYear}&nextMonth=${currentMonth+1}"
										class="month-tooltip" data-toggle="tooltip"
										data-placement="right" title="Next Month"><i
										class="fas fa-angle-double-right"></i></a>
								</c:if>
								<c:if test="${currentMonth == 12}">
									<a href="IndexServlet?selectYear=${currentYear}&priMonth=11"
										class="month-tooltip" data-toggle="tooltip"
										data-placement="left" title="Privious Month"><i
										class="fas fa-angle-double-left"></i></a>
									${currentMonth}月
									<a href="IndexServlet?selectYear=${currentYear+1}&nextMonth=1"
										class="month-tooltip" data-toggle="tooltip"
										data-placement="right" title="Next Month"><i
										class="fas fa-angle-double-right"></i></a>
								</c:if>
								<c:if test="${currentMonth == 1}">
									<a href="IndexServlet?selectYear=${currentYear-1}&priMonth=12"
										class="month-tooltip" data-toggle="tooltip"
										data-placement="left" title="Privious Month"><i
										class="fas fa-angle-double-left"></i></a>
									${currentMonth}月
									<a href="IndexServlet?selectYear=${currentYear}&nextMonth=2"
										class="month-tooltip" data-toggle="tooltip"
										data-placement="right" title="Next Month"><i
										class="fas fa-angle-double-right"></i></a>
								</c:if>
							</h4>
							<table class="calender">
								<thead>
									<tr>
										<c:forEach var="var1" items="${space1}">
											<th scope="col" class="center">${var1}</th>
										</c:forEach>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="var2" items="${space2}">
										<th scope="col" class="center"><a
											href="IndexServlet?date=${selectM}/${var2}">${var2}</a></th>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<c:choose>
							<c:when test="${date == null}">
								<div class="card card-container">
									<div class="card-content">
										<h4 style="font-size: 1.1em;">本日の予定</h4>
										<br>
										<c:if test="${errMsg1 != null}">
											<hr>
											<p style="text-align: center;">${errMsg1}</p>
										</c:if>
										<c:if test="${errMsg1 == null}">
											<form action="IndexServlet" method="POST">
												<table class="table">
													<thead>
														<tr>
															<th scope="col"></th>
															<th scope="col" class="center">日付</th>
															<th scope="col" class="center">時間</th>
															<th scope="col" class="center">タイトル</th>
															<th scope="col" class="center">済</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach var="schedule" items="${todaySchedules}"
															varStatus="status">
															<c:if test="${schedule.checked == 1}">
																<tr style="text-decoration: line-through;">
																	<th class="row"><a
																		href="ScheduleDetailServlet?id=${schedule.scheduleId}">
																			<i class="fas fa-info-circle my-color"></i>
																	</a></th>
																	<td class="center">${schedule.scheduleDate}</td>
																	<td class="center">${schedule.scheduleTime}</td>
																	<td class="center">${schedule.scheduleTitle}</td>
																	<td><i
																		class="fas fa-check-square form-check-input cb"
																		style="color: #007bff;"></i></td>
															</c:if>
															<c:if test="${schedule.checked == 0}">
																<tr>
																	<th class="row"><a
																		href="ScheduleDetailServlet?id=${schedule.scheduleId}">
																			<i class="fas fa-info-circle my-color"></i>
																	</a></th>
																	<td class="center">${schedule.scheduleDate}</td>
																	<td class="center">${schedule.scheduleTime}</td>
																	<td class="center">${schedule.scheduleTitle}</td>
																	<td><input class="form-check-input cb"
																		type="checkbox" name="checked"
																		value="${schedule.scheduleId}"></td>
															</c:if>
														</c:forEach>
													</tbody>
												</table>
												<div class="row detail">
													<div class="col s5"></div>
													<div class="col s2 btn-update">
														<button type="submit" class="btn btn-info">更新</button>
													</div>
													<div class="col s5"></div>
												</div>
											</form>
										</c:if>
									</div>
								</div>
							</c:when>
							<c:when test="${date != null}">
								<div class="card card-container">
									<div class="card-content">
										<h4 style="font-size: 1.1em;">${date}の予定</h4>
										<br>
										<c:if test="${errMsg2 != null}">
											<hr>
											<p style="text-align: center;">${errMsg2}</p>
										</c:if>
										<c:if test="${errMsg2 == null}">
											<form action="IndexServlet" method="POST" name="checked">
												<table class="table">
													<thead>
														<tr>
															<th scope="col"></th>
															<th scope="col" class="center">日付</th>
															<th scope="col" class="center">時間</th>
															<th scope="col" class="center">タイトル</th>
															<th scope="col" class="center">済</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach var="schedule" items="${thatDateSchedules}">
															<c:if test="${schedule.checked == 1}">
																<tr style="text-decoration: line-through;">
																	<th class="row"><a
																		href="ScheduleDetailServlet?id=${schedule.scheduleId}">
																			<i class="fas fa-info-circle my-color"></i>
																	</a></th>
																	<td class="center">${schedule.scheduleDate}</td>
																	<td class="center">${schedule.scheduleTime}</td>
																	<td class="center">${schedule.scheduleTitle}</td>
																	<td><i
																		class="fas fa-check-square form-check-input cb"
																		style="color: #007bff;"></i></td>
															</c:if>
															<c:if test="${schedule.checked == 0}">
																<tr>
																	<th class="row"><a
																		href="ScheduleDetailServlet?id=${schedule.scheduleId}">
																			<i class="fas fa-info-circle my-color"></i>
																	</a></th>
																	<td class="center">${schedule.scheduleDate}</td>
																	<td class="center">${schedule.scheduleTime}</td>
																	<td class="center">${schedule.scheduleTitle}</td>
																	<td><input class="form-check-input cb"
																		type="checkbox" name="checked"
																		value="${schedule.scheduleId}"></td>
															</c:if>
														</c:forEach>
													</tbody>
												</table>
												<div class="row detail">
													<div class="col s5"></div>
													<div class="col s2 btn-update">
														<button type="submit" class="btn btn-info"
															onclick="clickBtn5()">更新</button>
													</div>
													<div class="col s5"></div>
												</div>
											</form>
										</c:if>
									</div>
								</div>
							</c:when>
						</c:choose>
					</div>
					<div class="col"></div>
				</div>
			</div>
		</c:when>
		<c:when test="${priMonth != null && nextMonth == null}">
			<div class="container">
				<div class="row">
					<div class="col"></div>
					<div class="col-10">
						<div class="card card-container">
							<h4>${selectYear}年</h4>
							<h4>
								<c:if test="${priMonth != 12 && priMonth != 1}">
									<a
										href="IndexServlet?selectYear=${selectYear}&priMonth=${priMonth-1}"
										class="month-tooltip" data-toggle="tooltip"
										data-placement="left" title="Privious Month"><i
										class="fas fa-angle-double-left"></i></a>
									${priMonth}月
									<a
										href="IndexServlet?selectYear=${selectYear}&nextMonth=${priMonth+1}"
										class="month-tooltip" data-toggle="tooltip"
										data-placement="right" title="Next Month"><i
										class="fas fa-angle-double-right"></i></a>
								</c:if>
								<c:if test="${priMonth == 12}">
									<a href="IndexServlet?selectYear=${selectYear}&priMonth=11"
										class="month-tooltip" data-toggle="tooltip"
										data-placement="left" title="Privious Month"><i
										class="fas fa-angle-double-left"></i></a>
									${priMonth}月
									<a href="IndexServlet?selectYear=${selectYear+1}&nextMonth=1"
										class="month-tooltip" data-toggle="tooltip"
										data-placement="right" title="Next Month"><i
										class="fas fa-angle-double-right"></i></a>
								</c:if>
								<c:if test="${priMonth == 1}">
									<a href="IndexServlet?selectYear=${selectYear-1}&priMonth=12"
										class="month-tooltip" data-toggle="tooltip"
										data-placement="left" title="Privious Month"><i
										class="fas fa-angle-double-left"></i></a>
									${priMonth}月
									<a href="IndexServlet?selectYear=${selectctYear}&nextMonth=2"
										class="month-tooltip" data-toggle="tooltip"
										data-placement="right" title="Next Month"><i
										class="fas fa-angle-double-right"></i></a>
								</c:if>
							</h4>
							<table class="calender">
								<thead>
									<tr>
										<c:forEach var="var1" items="${space1}">
											<th scope="col" class="center">${var1}</th>
										</c:forEach>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="var2" items="${space2}">
										<th scope="col" class="center"><a
											href="IndexServlet?date=${selectYear}/${priMonth}/${var2}&selectYear=${selectYear}&priMonth=${priMonth}">${var2}</a></th>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<c:choose>
							<c:when test="${date != null}">
								<div class="card card-container">
									<div class="card-content">
										<h4 style="font-size: 1.1em;">${date}の予定</h4>
										<br>
										<c:if test="${errMsg2 != null}">
											<hr>
											<p style="text-align: center;">${errMsg2}</p>
										</c:if>
										<c:if test="${errMsg2 == null}">
											<form action="IndexServlet" method="POST">
												<table class="table">
													<thead>
														<tr>
															<th scope="col"></th>
															<th scope="col" class="center">日付</th>
															<th scope="col" class="center">時間</th>
															<th scope="col" class="center">タイトル</th>
															<th scope="col" class="center">済</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach var="schedule" items="${thatDateSchedules}">
															<c:if test="${schedule.checked == 1}">
																<tr style="text-decoration: line-through;">
																	<th class="row"><a
																		href="ScheduleDetailServlet?id=${schedule.scheduleId}">
																			<i class="fas fa-info-circle my-color"></i>
																	</a></th>
																	<td class="center">${schedule.scheduleDate}</td>
																	<td class="center">${schedule.scheduleTime}</td>
																	<td class="center">${schedule.scheduleTitle}</td>
																	<td><i
																		class="fas fa-check-square form-check-input cb"
																		style="color: #007bff;"></i></td>
															</c:if>
															<c:if test="${schedule.checked == 0}">
																<tr>
																	<th class="row"><a
																		href="ScheduleDetailServlet?id=${schedule.scheduleId}">
																			<i class="fas fa-info-circle my-color"></i>
																	</a></th>
																	<td class="center">${schedule.scheduleDate}</td>
																	<td class="center">${schedule.scheduleTime}</td>
																	<td class="center">${schedule.scheduleTitle}</td>
																	<td><input class="form-check-input cb"
																		type="checkbox" name="checked"
																		value="${schedule.scheduleId}"></td>
															</c:if>
														</c:forEach>
													</tbody>
												</table>
												<div class="row detail">
													<div class="col s5"></div>
													<div class="col s2 btn-update">
														<button type="submit" class="btn btn-info">更新</button>
													</div>
													<div class="col s5"></div>
												</div>
											</form>
										</c:if>
									</div>
								</div>
							</c:when>
						</c:choose>
					</div>
					<div class="col"></div>
				</div>
			</div>
		</c:when>
		<c:when test="${priMonth == null && nextMonth != null}">
			<div class="container">
				<div class="row">
					<div class="col"></div>
					<div class="col-10">
						<div class="card card-container">
							<h4>${selectYear}年</h4>
							<h4>
								<c:if test="${nextMonth != 12 && nextMonth !=1}">
									<a
										href="IndexServlet?selectYear=${selectYear}&priMonth=${nextMonth-1}"
										class="month-tooltip" data-toggle="tooltip"
										data-placement="left" title="Privious Month"><i
										class="fas fa-angle-double-left"></i></a>
									${nextMonth}月
									<a
										href="IndexServlet?selectYear=${selectYear}&nextMonth=${nextMonth+1}"
										class="month-tooltip" data-toggle="tooltip"
										data-placement="right" title="Next Month"><i
										class="fas fa-angle-double-right"></i></a>
								</c:if>
								<c:if test="${nextMonth == 12}">
									<a href="IndexServlet?selectYear=${selectYear}&priMonth=11"
										class="month-tooltip" data-toggle="tooltip"
										data-placement="left" title="Privious Month"><i
										class="fas fa-angle-double-left"></i></a>
									${nextMonth}月
									<a href="IndexServlet?selectYear=${selectYear+1}&nextMonth=1"
										class="month-tooltip" data-toggle="tooltip"
										data-placement="right" title="Next Month"><i
										class="fas fa-angle-double-right"></i></a>
								</c:if>
								<c:if test="${nextMonth ==1}">
									<a href="IndexServlet?selectYear=${selectYear-1}&priMonth=12"
										class="month-tooltip" data-toggle="tooltip"
										data-placement="left" title="Privious Month"><i
										class="fas fa-angle-double-left"></i></a>
									${nextMonth}月
									<a href="IndexServlet?selectYear=${selectYear}&nextMonth=2"
										class="month-tooltip" data-toggle="tooltip"
										data-placement="right" title="Next Month"><i
										class="fas fa-angle-double-right"></i></a>
								</c:if>
							</h4>
							<table class="calender">
								<thead>
									<tr>
										<c:forEach var="var1" items="${space1}">
											<th scope="col" class="center">${var1}</th>
										</c:forEach>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="var2" items="${space2}">
										<th scope="col" class="center"><a
											href="IndexServlet?date=${selectYear}/${nextMonth}/${var2}&selectYear=${selectYear}&nextMonth=${nextMonth}">${var2}</a></th>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<c:choose>
							<c:when test="${date != null}">
								<div class="card card-container">
									<div class="card-content">
										<h4 style="font-size: 1.1em;">${date}の予定</h4>
										<br>
										<c:if test="${errMsg2 != null}">
											<hr>
											<p style="text-align: center;">${errMsg2}</p>
										</c:if>
										<c:if test="${errMsg2 == null}">
											<form action="IndexServlet" method="POST">
												<table class="table">
													<thead>
														<tr>
															<th scope="col"></th>
															<th scope="col" class="center">日付</th>
															<th scope="col" class="center">時間</th>
															<th scope="col" class="center">タイトル</th>
															<th scope="col" class="center">済</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach var="schedule" items="${thatDateSchedules}">
															<c:if test="${schedule.checked == 1}">
																<tr style="text-decoration: line-through;">
																	<th class="row"><a
																		href="ScheduleDetailServlet?id=${schedule.scheduleId}">
																			<i class="fas fa-info-circle my-color"></i>
																	</a></th>
																	<td class="center">${schedule.scheduleDate}</td>
																	<td class="center">${schedule.scheduleTime}</td>
																	<td class="center">${schedule.scheduleTitle}</td>
																	<td><i
																		class="fas fa-check-square form-check-input cb"
																		style="color: #007bff;"></i></td>
															</c:if>
															<c:if test="${schedule.checked == 0}">
																<tr>
																	<th class="row"><a
																		href="ScheduleDetailServlet?id=${schedule.scheduleId}">
																			<i class="fas fa-info-circle my-color"></i>
																	</a></th>
																	<td class="center">${schedule.scheduleDate}</td>
																	<td class="center">${schedule.scheduleTime}</td>
																	<td class="center">${schedule.scheduleTitle}</td>
																	<td><input class="form-check-input cb"
																		type="checkbox" name="checked"
																		value="${schedule.scheduleId}"></td>
															</c:if>
														</c:forEach>
													</tbody>
												</table>
												<div class="row detail">
													<div class="col s5"></div>
													<div class="col s2 btn-update">
														<button type="submit" class="btn btn-info">更新</button>
													</div>
													<div class="col s5"></div>
												</div>
											</form>
										</c:if>
									</div>
								</div>
							</c:when>
						</c:choose>
					</div>
					<div class="col"></div>
				</div>
			</div>
		</c:when>
	</c:choose>
</body>
<script>
	$(":checkbox").click(function() {
		if ($(this).is(":checked")) {
			$(this).closest("tr").css("text-decoration", "line-through");
		} else {
			$(this).closest("tr").css("text-decoration", "none");
		}
	});
</script>
<script>
	$(function() {
		$('[data-toggle="tooltip"]').tooltip()
	})
</script>

</html>