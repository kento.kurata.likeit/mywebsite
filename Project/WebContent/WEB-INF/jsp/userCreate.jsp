<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link href="css/original/common.css" rel="stylesheet">
<title>新規登録画面</title>
</head>

<body>
	<nav class="navbar">
		<a href="LoginServlet" class="logo">To Do Schedule</a>
	</nav>
	<div class="container">
		<div class="row">
			<div class="col"></div>
			<div class="col-5">
				<h2>ユーザー新規登録</h2>
				<c:if test="${errMsg1 != null}">
					<p style="color: #ff3366; text-align: center;">${errMsg1}</p>
				</c:if>
				<c:if test="${errMsg2 != null}">
					<p style="color: #ff3366; text-align: center;">${errMsg2}</p>
				</c:if>
				<c:if test="${errMsg3 != null}">
					<p style="color: #ff3366; text-align: center;">${errMsg3}</p>
				</c:if>
				<div class="card card-container card-login">
					<form action="UserCreateServlet" method="POST">
						<div class="row detail">
							<label for="name">名前</label><input type="text" name="name" id="name">
						</div>
						<div class="row detail">
							<label for="loginId">ログインID</label><input type="text" name="loginId" id="loginId">
						</div>
						<div class="row detail">
							<label for="password1">パスワード</label><input type="password" name="password1"  id="password1">
						</div>
						<div class="row detail">
							<label for="password2">パスワード(確認)</label><input type="password" name="password2" id="password2">
						</div>
						<div class="row detail">
							<button class="btn btn-info login" type="submit">登録</button>
						</div>
					</form>
				</div>
			</div>
			<div class="col"></div>
		</div>
	</div>
</body>

</html>