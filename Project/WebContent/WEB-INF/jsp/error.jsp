<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link href="css/original/common.css" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <title>エラー画面</title>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
</head>

<body>
    <header>
        <nav class="navbar">
		<a href="LoginServlet" class="logo">To Do Schedule</a>
	</nav>
    </header>
    <div class="container">
        <div class="row">
            <div class="col"></div>
            <div class="col-10">
                <div class="card card-container">
                    <h4>システムエラーが発生しました</h4>
                    <h5>不正なアクセス</h5>
                    <div class="row detail">
                        <div class="col s5"></div>
                        <div class="col s2 btn-update">
                            <a class="btn btn-info" href="IndexServlet" style="color: #ffffff;">TOPページへ</a>
                        </div>
                        <div class="col s5"></div>
                    </div>
                </div>
            </div>
            <div class="col"></div>
        </div>
    </div>
</body>

</html>