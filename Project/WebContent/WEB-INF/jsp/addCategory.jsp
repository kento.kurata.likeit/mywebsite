<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link href="css/original/common.css" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	rel="stylesheet">
<title>カテゴリー追加画面</title>
<script>
	$(function() {
		$('[data-toggle="tooltip"]').tooltip()
	})
</script>

</head>

<body>
	<header>
		<nav class="navbar">
			<div class="header-logo">
				<div class="btn-group">
					<a class="btn" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false"> <i class="fas fa-bars fa-lg my-big"></i>
					</a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="IndexServlet"><i
							class="fas fa-home my-big"></i>Home</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item"
							href="UserDetailServlet?id=${loginInfo.id}"><i
							class="fas fa-user-circle my-big"></i>User Information</a> <a
							class="dropdown-item"
							href="AddCategoryServlet?id=${loginInfo.id}"><i
							class="fas fa-cog my-big"></i>Add Category</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item"
							href="SerchScheduleServlet?id=${loginInfo.id}"><i
							class="fas fa-search my-big"></i>Search Schedule</a> <a
							class="dropdown-item"
							href="AddScheduleServlet?id=${loginInfo.id}"><i
							class="fas fa-plus my-big"></i>Add Schedule</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="LogoutServlet"><i
							class="fas fa-sign-out-alt my-big"></i>Logout</a>
					</div>
				</div>
				<a href="IndexServlet" class="logo">To Do Schedule</a>
			</div>
			<div class="header-icons">
				<a href="IndexServlet" class="home" data-toggle="tooltip"
					data-placement="left" title="Home"><i
					class="fas fa-home my-big"></i></a> <a
					href="SerchScheduleServlet?id=${loginInfo.id}" class="search"
					data-toggle="tooltip" data-placement="left" title="Search Schedule"><i
					class="fas fa-search my-big"></i></a> <a
					href="AddScheduleServlet?id=${loginInfo.id}" class="new-schedule"
					data-toggle="tooltip" data-placement="left" title="Add Schedule"><i
					class="fas fa-plus my-big"></i></a> <a href="LogoutServlet"
					class="logout" data-toggle="tooltip" data-placement="left"
					title="Logout"><i class="fas fa-sign-out-alt my-big"></i></a>
			</div>
		</nav>
	</header>
	<div class="container">
		<div class="row">
			<div class="col"></div>
			<div class="col-10">
				<div class="card card-container">
					<h4>カテゴリー追加登録</h4>
					<br>
					<c:if test="${errMsg1 != null}">
						<p style="color: #ff3366; text-align: center;">${errMsg1}</p>
					</c:if>
					<c:if test="${errMsg2 != null}">
						<p style="color: #ff3366; text-align: center;">${errMsg2}</p>
					</c:if>
					<form action="AddCategoryServlet" method="POST">
						<input type="hidden" name="id" id="id" value="${loginInfo.id}">
						<br>
						<div class="row detail">
							<div class="col"></div>
							<div class="inpur-field col s6">
								<label for="categoryName">カテゴリー名</label> <br> <input
									type="text" name="categoryName" id="categoryName"
									class="category" style="text-align: center;">
							</div>
							<div class="col"></div>
						</div>
						<div class="row detail">
							<div class="col s5"></div>
							<div class="col s2 btn-update">
								<button type="submit" class="btn btn-info">追加</button>
							</div>
							<div class="col s5"></div>
						</div>
					</form>
				</div>
				<div class="card card-container">
					<div class="card-content">
						<h4 style="font-size: 1.1em;">カテゴリーリスト</h4>
						<br>
						<table class="table">
							<thead>
								<tr>
									<th scope="col" class="center">カテゴリー名</th>
									<th scope="col" class="center">削除</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="category" items="${categories}">
									<tr>
										<td class="center">${category.categoryName}</td>
										<c:if test="${category.id > 3}">
											<td class="center"><a
												href="CategoryDeleteServlet?id=${category.id}"><i
													class="fas fa-minus-square my-big"></i></a></td>
										</c:if>
										<c:if test="${category.id <= 3}">
											<td class="center"></td>
										</c:if>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col"></div>
		</div>
	</div>
</body>

</html>