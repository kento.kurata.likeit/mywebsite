<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link href="css/original/common.css" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	rel="stylesheet">
<title>カテゴリー削除画面</title>
<script>
	$(function() {
		$('[data-toggle="tooltip"]').tooltip()
	})
</script>

</head>

<body>
	<header>
		<nav class="navbar">
			<div class="header-logo">
				<div class="btn-group">
					<a class="btn" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false"> <i class="fas fa-bars fa-lg my-big"></i>
					</a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="IndexServlet"><i class="fas fa-home my-big"></i>Home</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="UserDetailServlet?id=${loginInfo.id}"><i class="fas fa-user-circle my-big"></i>User Information</a>
						<a class="dropdown-item" href="AddCategoryServlet?id=${loginInfo.id}"><i class="fas fa-cog my-big"></i>Add Category</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="SerchScheduleServlet?id=${loginInfo.id}"><i class="fas fa-search my-big"></i>Search Schedule</a>
						<a class="dropdown-item" href="AddScheduleServlet?id=${loginInfo.id}"><i class="fas fa-plus my-big"></i>Add Schedule</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="LogoutServlet"><i class="fas fa-sign-out-alt my-big"></i>Logout</a>
					</div>
				</div>
				<a href="IndexServlet" class="logo">To Do Schedule</a>
			</div>
			<div class="header-icons">
				<a href="IndexServlet" class="home" data-toggle="tooltip" data-placement="left" title="Home"><i class="fas fa-home my-big"></i></a>
				<a href="SerchScheduleServlet?id=${loginInfo.id}" class="search" data-toggle="tooltip" data-placement="left" title="Search Schedule"><i class="fas fa-search my-big"></i></a>
				<a href="AddScheduleServlet?id=${loginInfo.id}" class="new-schedule" data-toggle="tooltip" data-placement="left" title="Add Schedule"><i class="fas fa-plus my-big"></i></a>
				<a href="LogoutServlet" class="logout" data-toggle="tooltip" data-placement="left" title="Logout"><i class="fas fa-sign-out-alt my-big"></i></a>
			</div>
		</nav>
	</header>
	<div class="container">
		<div class="row">
			<div class="col"></div>
			<div class="col-10">
				<div class="card card-container">
					<h4>カテゴリー削除</h4>
					<form action="CategoryDeleteServlet" method="POST">
						<input type="hidden" name="id" id="id" value="${loginInfo.id}">
						<br> <br>
						<div class="row detail">
							<div class="col"></div>
							<div class="inpur-field col s6">
								<p
									style="border-bottom: 1px solid #bbbbbb; color: #555555; text-align: center;">
									${category.categoryName}</p>
							</div>
							<div class="col"></div>
						</div>
						<h4>本当に削除しますか？</h4>
						<div class="row detail">
							<div class="col s3"></div>
							<div class="col s3">
								<button type="button" class="btn btn-info"
									onclick="history.back()">戻る</button>
							</div>
							<div class="col s3">
								<button type="submit" class="btn btn-danger">削除する</button>
							</div>
							<div class="col s3"></div>
						</div>
					</form>
				</div>
			</div>
			<div class="col"></div>
		</div>
	</div>
</body>

</html>