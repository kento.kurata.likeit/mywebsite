<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link href="css/original/common.css" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	rel="stylesheet">
<title>スケジュール検索画面</title>
</head>
<body>
	<header>
		<nav class="navbar">
			<div class="header-logo">
				<div class="btn-group">
					<a class="btn" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false"> <i class="fas fa-bars fa-lg my-big"></i>
					</a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="IndexServlet"><i
							class="fas fa-home my-big"></i>Home</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item"
							href="UserDetailServlet?id=${loginInfo.id}"><i
							class="fas fa-user-circle my-big"></i>User Information</a> <a
							class="dropdown-item"
							href="AddCategoryServlet?id=${loginInfo.id}"><i
							class="fas fa-cog my-big"></i>Add Category</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item"
							href="SerchScheduleServlet?id=${loginInfo.id}"><i
							class="fas fa-search my-big"></i>Search Schedule</a> <a
							class="dropdown-item"
							href="AddScheduleServlet?id=${loginInfo.id}"><i
							class="fas fa-plus my-big"></i>Add Schedule</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="LogoutServlet"><i
							class="fas fa-sign-out-alt my-big"></i>Logout</a>
					</div>
				</div>
				<a href="IndexServlet" class="logo">To Do Schedule</a>
			</div>
			<div class="header-icons">
				<a href="IndexServlet" class="home" data-toggle="tooltip"
					data-placement="left" title="Home"><i
					class="fas fa-home my-big"></i></a> <a
					href="SerchScheduleServlet?id=${loginInfo.id}" class="search"
					data-toggle="tooltip" data-placement="left" title="Search Schedule"><i
					class="fas fa-search my-big"></i></a> <a
					href="AddScheduleServlet?id=${loginInfo.id}" class="new-schedule"
					data-toggle="tooltip" data-placement="left" title="Add Schedule"><i
					class="fas fa-plus my-big"></i></a> <a href="LogoutServlet"
					class="logout" data-toggle="tooltip" data-placement="left"
					title="Logout"><i class="fas fa-sign-out-alt my-big"></i></a>
			</div>
		</nav>
	</header>
	<div class="container">
		<div class="row">
			<div class="col"></div>
			<div class="col-10">
				<div class="card card-container">
					<h4>スケジュール検索</h4>
					<form action="SerchScheduleServlet" method="POST">
						<input type="hidden" name="id" id="id" value="${loginInfo.id}">
						<br> <br>
						<div class="row">
							<div class="inpur-field col">
								<label>進捗状況</label>
								<div class="radioArea" id="makeImg">
									<input type="radio" name="checked" id="r1" value="1">
									<label for="r1">済</label>
									<input type="radio" name="checked" id="r2" value="0">
									<label for="r2">未</label>
									<input type="radio" name="checked" value="" checked="checked" style="display: none;" />
								</div>
							</div>
						</div>
						<div class="row detail">
							<div class="inpur-field col s4">
								<label for="date">日付</label> <input type="date" name="date"
									id="date">
							</div>
							<div class="inpur-field col s4">
								<label for="startTime">時間(以降)</label> <input type="time"
									name="startTime" id="startTime">
							</div>
							<div class="inpur-field col s4">
								<label for="finTime">時間(以前)</label> <input type="time"
									name="finTime" id="finTime">
							</div>
						</div>
						<div class="row detail">
							<div class="inpur-field col s12">
								<br> <label>カテゴリー</label> <select name="categoryId">
									<option hidden="hidden" value="">--選択して下さい--</option>
									<c:forEach var="category" items="${categories}">
										<option value="${category.id}">${category.categoryName}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<br>
						<div class="row detail">
							<div class="inpur-field col s12">
								<label for="title">タイトル</label> <input type="text" name="title"
									id="title">
							</div>
						</div>
						<div class="row detail">
							<div class="inpur-field col s12">
								<label for="memo">詳細・メモ</label>
								<textarea name="memo" id="memo" cols="30" rows="5"></textarea>
							</div>
						</div>
						<div class="row detail">
							<div class="col s5"></div>
							<div class="col s2 btn-update">
								<button type="submit" class="btn btn-info">検索する</button>
							</div>
							<div class="col s5"></div>
						</div>
					</form>
				</div>
				<c:choose>
					<c:when test="${errMsg1 == null}">
						<div class="card card-container">
							<h4 style="font-size: 1.1em;">検索結果</h4>
							<br>
							<table class="table">
								<thead>
									<tr>
										<th scope="col"></th>
										<th scope="col" class="center">日付</th>
										<th scope="col" class="center">時間</th>
										<th scope="col" class="center">カテゴリー</th>
										<th scope="col" class="center">タイトル</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="schedule" items="${schedules}">
										<c:if test="${schedule.checked == 1}">
											<tr style="text-decoration: line-through;">
												<th class="row"><a
													href="ScheduleDetailServlet?id=${schedule.scheduleId}">
														<i class="fas fa-info-circle my-color"></i>
												</a></th>
												<td class="center">${schedule.scheduleDate}</td>
												<td class="center">${schedule.scheduleTime}</td>
												<td class="center">${schedule.categoryName}</td>
												<td class="center">${schedule.scheduleTitle}</td>
											</tr>
										</c:if>
										<c:if test="${schedule.checked == 0}">
											<tr>
												<th class="row"><a
													href="ScheduleDetailServlet?id=${schedule.scheduleId}">
														<i class="fas fa-info-circle my-color"></i>
												</a></th>
												<td class="center">${schedule.scheduleDate}</td>
												<td class="center">${schedule.scheduleTime}</td>
												<td class="center">${schedule.categoryName}</td>
												<td class="center">${schedule.scheduleTitle}</td>
											</tr>
										</c:if>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</c:when>
					<c:when test="${errMsg1 != null}">
						<div class="card card-container">
							<h4 style="font-size: 1.1em;">検索結果</h4>
							<br>
							<hr>
							<p style="text-align: center;">${errMsg1}</p>
						</div>
					</c:when>
				</c:choose>
			</div>
			<div class="col"></div>
		</div>
	</div>
</body>
<script>
	$(function() {
		$('[data-toggle="tooltip"]').tooltip()
	})
</script>
</html>