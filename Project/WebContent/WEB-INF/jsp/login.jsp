<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link href="css/original/common.css" rel="stylesheet">

<title>ログイン画面</title>
</head>
<body>
	<nav class="navbar">
		<a href="LoginServlet" class="logo">To Do Schedule</a>
	</nav>
	<div class="container">
		<div class="row">
			<div class="col"></div>
			<div class="col-5">
				<div class="card card-container card-login" style="padding: 24px;">
					<p id="profile-name" class="profile-name-card"></p>
					<c:if test="${errMsg != null}">
						<p style="color: #ff3366; text-align: center;">${errMsg}</p>
					</c:if>
					<form class="form-signin" action="LoginServlet" method="post">
						<div class="row detail">
							<label>ログインID</label> <input type="text" name="loginId"
								id="loginId">
						</div>
						<div class="row detail">
							<label>パスワード</label> <input type="password" name="password"
								id="password">
						</div>
						<div class="row detail">
							<button class="btn btn-info login" type="submit">ログイン</button>
						</div>
						<br>
						<p class="or">or</p>
						<br> <a href="UserCreateServlet" class="newAcount">新規登録</a>
					</form>
				</div>
			</div>
			<div class="col"></div>
		</div>
	</div>
</body>
</html>