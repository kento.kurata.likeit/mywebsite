package beans;

import java.io.Serializable;

public class User implements Serializable {
	private String name;
	private String loginId;
	private String password;
	private int id;

	//コンストラクタ
	public User() {
		super();
	}

	//新規登録時確認用
	public User(String loginId) {
		super();
		this.loginId = loginId;
	}

	//詳細用
	public User(int id, String name, String loginId) {
		super();
		this.id = id;
		this.name = name;
		this.loginId = loginId;
	}

	//ログイン用
	public User(String loginId, String password) {
		super();
		this.loginId = loginId;
		this.password = password;
	}

	// 全てのコンストラクタ
	public User(String name, String loginId, String password, int id) {
		super();
		this.name = name;
		this.loginId = loginId;
		this.password = password;
		this.id = id;
	}

	//アクセサ
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}


}
