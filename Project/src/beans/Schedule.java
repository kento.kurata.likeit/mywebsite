package beans;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

public class Schedule implements Serializable {
	private int scheduleId;
	private Date scheduleDate;
	private Time scheduleTime;
	private String scheduleTitle;
	private String scheduleMemo;
	private int categoryId;
	private int userId;
	private String categoryName;
	private int checked;

	//コンストラクタ
	public Schedule() {
		super();
	}

	//全てのコンストラクタ
	public Schedule(int scheduleId, Date scheduleDate, Time scheduleTime, String scheduleTitle, String scheduleMemo,
			int categoryId, int userId, String categoryName, int checked) {
		super();
		this.scheduleId = scheduleId;
		this.scheduleDate = scheduleDate;
		this.scheduleTime = scheduleTime;
		this.scheduleTitle = scheduleTitle;
		this.scheduleMemo = scheduleMemo;
		this.categoryId = categoryId;
		this.userId = userId;
		this.categoryName = categoryName;
		this.checked = checked;
	}

	//今日
	public Schedule(int scheduleId, Date scheduleDate, Time scheduleTime, String scheduleTitle, String categoryName,
			int checked) {
		super();
		this.scheduleId = scheduleId;
		this.scheduleDate = scheduleDate;
		this.scheduleTime = scheduleTime;
		this.scheduleTitle = scheduleTitle;
		this.categoryName = categoryName;
		this.checked = checked;
	}

	//指定日スケジュール検索用
	public Schedule(int scheduleId, Date scheduleDate, Time scheduleTime, String scheduleTitle, String categoryName) {
		super();
		this.scheduleId = scheduleId;
		this.scheduleDate = scheduleDate;
		this.scheduleTime = scheduleTime;
		this.scheduleTitle = scheduleTitle;
		this.categoryName = categoryName;
	}

	//スケジュール検索用
	public Schedule(int scheduleId, Date scheduleDate, Time scheduleTime, String scheduleTitle, String scheduleMemo,
			String categoryName, int checked) {
		super();
		this.scheduleId = scheduleId;
		this.scheduleDate = scheduleDate;
		this.scheduleTime = scheduleTime;
		this.scheduleTitle = scheduleTitle;
		this.scheduleMemo = scheduleMemo;
		this.categoryName = categoryName;
		this.checked = checked;
	}

	//アクセサ
	public int getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(int scheduleId) {
		this.scheduleId = scheduleId;
	}

	public Date getScheduleDate() {
		return scheduleDate;
	}

	public void setScheduleDate(Date scheduleDate) {
		this.scheduleDate = scheduleDate;
	}

	public Time getScheduleTime() {
		return scheduleTime;
	}

	public void setScheduleTime(Time scheduleTime) {
		this.scheduleTime = scheduleTime;
	}

	public String getScheduleTitle() {
		return scheduleTitle;
	}

	public void setScheduleTitle(String scheduleTitle) {
		this.scheduleTitle = scheduleTitle;
	}

	public String getScheduleMemo() {
		return scheduleMemo;
	}

	public void setScheduleMemo(String scheduleMemo) {
		this.scheduleMemo = scheduleMemo;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public int getChecked() {
		return checked;
	}

	public void setChecked(int checked) {
		this.checked = checked;
	}
}
