package beans;

import java.io.Serializable;

public class Category implements Serializable {
	private int id;
	private String categoryName;
	private int createUserId;

	public Category() {
		super();
	}

	public Category(String categoryName) {
		super();
		this.categoryName = categoryName;
	}

	public Category(int id, String categoryName, int createUserId) {
		super();
		this.id = id;
		this.categoryName = categoryName;
		this.createUserId = createUserId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public int getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(int createUserId) {
		this.createUserId = createUserId;
	}


}
