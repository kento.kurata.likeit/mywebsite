package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import dao.UserDao;

/**
 * Servlet implementation class UserCreateServlet
 */
@WebServlet("/UserCreateServlet")
public class UserCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//userCreate.jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// リクエストパラメータ文字コードを指定
		request.setCharacterEncoding("UTF-8");
		try {
			// リクエストパラメータの入力項目を取得
			String name = request.getParameter("name");
			String loginId = request.getParameter("loginId");
			String password1 = request.getParameter("password1");
			String password2 = request.getParameter("password2");

			//何かしら空欄
			if (name.equals("") ||
					loginId.equals("") ||
					password1.equals("") ||
					password2.equals("")) {

				request.setAttribute("errMsg2", "全ての項目を入力してください。");

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
				dispatcher.forward(request, response);
				return;
			}

			//パスワード不一致
			if (!(password1.equals(password2))) {

				request.setAttribute("errMsg1", "パスワードが不一致です。");

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
				dispatcher.forward(request, response);
				return;
			}

			//ログインIDが同じ場合
			UserDao userDao = new UserDao();
			User user = userDao.findLoginId(loginId);
			if (!(user == null)) {
				request.setAttribute("errMsg3", "既にログインIDが使用されています。");

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
				dispatcher.forward(request, response);
				return;
			}

			UserDao userDao2 = new UserDao();
			userDao2.userCreate(name, loginId, password1);

			//ログイン画面にリダイレクト
			response.sendRedirect("LoginServlet");

		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("ErrorServlet");
		}
	}
}
