package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Category;
import beans.User;
import dao.CategoryDao;

/**
 * Servlet implementation class AddCategoryServlet
 */
@WebServlet("/AddCategoryServlet")
public class AddCategoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddCategoryServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			User loginInfo = (User) session.getAttribute("loginInfo");

			if (loginInfo == null) {
				response.sendRedirect("LoginServlet");
			} else {

				CategoryDao categoryDao = new CategoryDao();
				List<Category> categories = categoryDao.findCategories(String.valueOf(loginInfo.getId()));

				request.setAttribute("categories", categories);

				//addCategory.jspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/addCategory.jsp");
				dispatcher.forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("Error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータ文字コードを指定
		request.setCharacterEncoding("UTF-8");
		try {
			String createUserId = request.getParameter("id");
			String categoryName = request.getParameter("categoryName");

			if (categoryName.equals("")) {
				request.setAttribute("errMsg1", "入力してください。");

				CategoryDao categoryDao = new CategoryDao();
				List<Category> categories = categoryDao.findCategories(createUserId);

				request.setAttribute("categories", categories);

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/addCategory.jsp");
				dispatcher.forward(request, response);
				return;
			}

			CategoryDao categoryDao = new CategoryDao();
			Category category = categoryDao.findCateName(categoryName);

			if (!(category == null)) {
				request.setAttribute("errMsg2", "既に作成済みです。");

				CategoryDao categoryDao1 = new CategoryDao();
				List<Category> categories = categoryDao1.findCategories(createUserId);

				request.setAttribute("categories", categories);

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/addCategory.jsp");
				dispatcher.forward(request, response);
				return;
			}

			CategoryDao categoryDao1 = new CategoryDao();
			categoryDao1.addCategory(categoryName, createUserId);

			CategoryDao categoryDao2 = new CategoryDao();
			List<Category> categories = categoryDao2.findCategories(createUserId);

			request.setAttribute("categories", categories);

			//addCategory.jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/addCategory.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("Error");
		}
	}
}
