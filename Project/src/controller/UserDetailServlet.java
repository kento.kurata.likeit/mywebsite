package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Schedule;
import beans.User;
import dao.ScheduluDao;
import dao.UserDao;

/**
 * Servlet implementation class UserDetail
 */
@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		try {
			User loginInfo = (User) session.getAttribute("loginInfo");

			if (loginInfo == null) {
				response.sendRedirect("LoginServlet");
			} else {
				String id = request.getParameter("id");

				System.out.println(id);

				UserDao userDao = new UserDao();
				User user = userDao.ucerDetail(id);

				HttpSession session1 = request.getSession();
				session1.setAttribute("userDetail", user);

				ScheduluDao scheduluDao = new ScheduluDao();
				List<Schedule> todaySchedules = scheduluDao.todaySchedules(String.valueOf(loginInfo.getId()));

				request.setAttribute("todaySchedules", todaySchedules);

				System.out.println(todaySchedules);

				if (todaySchedules == null || todaySchedules.size() == 0) {
					request.setAttribute("errMsg3", "本日の予定はありません");
				}

				//userDetail.jspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDetail.jsp");
				dispatcher.forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("ErrorServlet");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータ文字コードを指定
		request.setCharacterEncoding("UTF-8");
		try {
			String id = request.getParameter("id");
			String name = request.getParameter("name");
			String password1 = request.getParameter("password1");
			String password2 = request.getParameter("password2");

			if (!password1.equals(password2)) {
				request.setAttribute("errMsg1", "パスワードが不一致です。");

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDetail.jsp");
				dispatcher.forward(request, response);
				return;
			}

			if (name.equals("")) {
				request.setAttribute("errMsg2", "名前を埋めてください。");

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDetail.jsp");
				dispatcher.forward(request, response);
				return;
			}

			UserDao userDao = new UserDao();
			String secret = userDao.secret(password1);

			UserDao userDao1 = new UserDao();
			userDao1.userUpdate(name, secret, id);

			response.sendRedirect("IndexServlet");
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("ErrorServlet");
		}
	}
}
