package controller;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Schedule;
import beans.User;
import dao.CalendarDao;
import dao.ScheduluDao;

/**
 * Servlet implementation class IndexServlet
 */
@WebServlet("/IndexServlet")
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		try {
			User loginInfo = (User) session.getAttribute("loginInfo");

			String date = request.getParameter("date");
			System.out.println(date + "日");
			request.setAttribute("date", date);

			String selectYear = request.getParameter("selectYear");
			request.setAttribute("selectYear", selectYear);
			System.out.println(selectYear + ":selectYear");

			String priMonth = request.getParameter("priMonth");
			request.setAttribute("priMonth", priMonth);
			System.out.println(priMonth + ":priMonth");

			String nextMonth = request.getParameter("nextMonth");
			request.setAttribute("nextMonth", nextMonth);
			System.out.println(nextMonth + ":nextMont");

			if (loginInfo == null) {
				response.sendRedirect("LoginServlet");
			} else {
				// 現在日時情報で初期化されたインスタンスの取得
				LocalDateTime nowDateTime = LocalDateTime.now();
				DateTimeFormatter thisyear = DateTimeFormatter.ofPattern("yyyy");
				// 日時情報を指定フォーマットの文字列で取得
				String currentYear = nowDateTime.format(thisyear);
				System.out.print(currentYear + "年");
				HttpSession session2 = request.getSession();
				session2.setAttribute("currentYear", currentYear);

				// 現在日時情報で初期化されたインスタンスの取得
				DateTimeFormatter thisMonth = DateTimeFormatter.ofPattern("MM");
				// 日時情報を指定フォーマットの文字列で取得
				String currentMonth = nowDateTime.format(thisMonth);
				System.out.print(currentMonth + "月");
				HttpSession session3 = request.getSession();
				session3.setAttribute("currentMonth", currentMonth);

				// 現在日時情報で初期化されたインスタンスの取得
				DateTimeFormatter thisM = DateTimeFormatter.ofPattern("yyyy/MM");
				// 日時情報を指定フォーマットの文字列で取得
				String selectM = nowDateTime.format(thisM);
				HttpSession session4 = request.getSession();
				session4.setAttribute("selectM", selectM);

				DateTimeFormatter today = DateTimeFormatter.ofPattern("yyyy/MM/dd");
				String toDay = nowDateTime.format(today);
				HttpSession session5 = request.getSession();
				session5.setAttribute("toDay", toDay);

				int lastDayTbl[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
				List<Object> list1 = new ArrayList<Object>();
				List<Object> list2 = new ArrayList<Object>();

				char dateOfWeekTbl[] = { '日', '月', '火', '水', '木', '金', '土' };

				if ((priMonth == null && nextMonth == null)
						|| (priMonth == currentMonth || nextMonth == currentMonth)) {
					try {
						Calendar calendar = Calendar.getInstance();
						int day = calendar.get(Calendar.DATE);
						int y = Integer.parseInt(currentYear);
						int m = Integer.parseInt(currentMonth);

						CalendarDao calendarDao = new CalendarDao();

						int dayOfWeek = calendarDao.getDayOfWeek(y, m, 1);
						int lastDay = lastDayTbl[m - 1];

						if (m == 2 && calendarDao.isLeepYear(y) != 0) {
							lastDay = 29;
						}
						for (int w = 0; w < 7; w++) {
							list1.add(dateOfWeekTbl[w]);
						}
						request.setAttribute("space1", list1);

						for (int u = 0; u < dayOfWeek; u++) {
							list2.add("");
						}
						for (day = 1; day <= lastDay; day++, dayOfWeek++) {
							list2.add(day);
							if (dayOfWeek % 7 == 6) {
								list2.add("</tr><tr>");
							}
						}
						request.setAttribute("space2", list2);

					} catch (Exception e) {
						throw new ServletException(e);
					}
					if (date == null) {
						ScheduluDao scheduluDao = new ScheduluDao();
						List<Schedule> todaySchedules = scheduluDao.todaySchedules(String.valueOf(loginInfo.getId()));

						request.setAttribute("todaySchedules", todaySchedules);

						if (todaySchedules == null || todaySchedules.size() == 0) {
							request.setAttribute("errMsg1", "本日の予定はありません");
						}

						//index.jspにフォワード
						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
						dispatcher.forward(request, response);

					} else if (date != null) {
						ScheduluDao scheduluDao = new ScheduluDao();
						List<Schedule> thatDateSchedules = scheduluDao.thatDateSchedules(
								String.valueOf(loginInfo.getId()),
								date);

						request.setAttribute("thatDateSchedules", thatDateSchedules);

						if (thatDateSchedules == null || thatDateSchedules.size() == 0) {
							request.setAttribute("errMsg2", "予定はありません");
						}
						//index.jspにフォワード
						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
						dispatcher.forward(request, response);
					}
				} else if ((priMonth != null && nextMonth == null) && priMonth != currentMonth) {
					try {
						Calendar calendar = Calendar.getInstance();
						int day = calendar.get(Calendar.DATE);
						int y = Integer.parseInt(selectYear);
						int m = Integer.parseInt(priMonth);

						CalendarDao calendarDao = new CalendarDao();

						int dayOfWeek = calendarDao.getDayOfWeek(y, m, 1);
						int lastDay = lastDayTbl[m - 1];

						if (m == 2 && calendarDao.isLeepYear(y) != 0) {
							lastDay = 29;
						}
						for (int w = 0; w < 7; w++) {
							list1.add(dateOfWeekTbl[w]);
						}
						request.setAttribute("space1", list1);

						for (int u = 0; u < dayOfWeek; u++) {
							list2.add("");
						}
						for (day = 1; day <= lastDay; day++, dayOfWeek++) {
							list2.add(day);
							if (dayOfWeek % 7 == 6) {
								list2.add("</tr><tr>");
							}
						}
						request.setAttribute("space2", list2);

					} catch (Exception e) {
						throw new ServletException(e);
					}
					if (date == null) {
						//index.jspにフォワード
						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
						dispatcher.forward(request, response);

					} else if (date != null) {
						ScheduluDao scheduluDao = new ScheduluDao();
						List<Schedule> thatDateSchedules = scheduluDao.thatDateSchedules(
								String.valueOf(loginInfo.getId()),
								date);
						request.setAttribute("thatDateSchedules", thatDateSchedules);

						if (thatDateSchedules == null || thatDateSchedules.size() == 0) {
							request.setAttribute("errMsg2", "予定はありません");
						}
						//index.jspにフォワード
						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
						dispatcher.forward(request, response);
					}
				} else if ((priMonth == null && nextMonth != null) && nextMonth != currentMonth) {
					try {
						Calendar calendar = Calendar.getInstance();
						int day = calendar.get(Calendar.DATE);
						int y = Integer.parseInt(selectYear);
						int m = Integer.parseInt(nextMonth);

						CalendarDao calendarDao = new CalendarDao();

						int dayOfWeek = calendarDao.getDayOfWeek(y, m, 1);
						int lastDay = lastDayTbl[m - 1];

						if (m == 2 && calendarDao.isLeepYear(y) != 0) {
							lastDay = 29;
						}
						for (int w = 0; w < 7; w++) {
							list1.add(dateOfWeekTbl[w]);
						}
						request.setAttribute("space1", list1);

						for (int u = 0; u < dayOfWeek; u++) {
							list2.add("");
						}
						for (day = 1; day <= lastDay; day++, dayOfWeek++) {
							list2.add(day);
							if (dayOfWeek % 7 == 6) {
								list2.add("</tr><tr>");
							}
						}
						request.setAttribute("space2", list2);

					} catch (Exception e) {
						throw new ServletException(e);
					}
					if (date == null) {
						//index.jspにフォワード
						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
						dispatcher.forward(request, response);

					} else if (date != null) {
						ScheduluDao scheduluDao = new ScheduluDao();
						List<Schedule> thatDateSchedules = scheduluDao.thatDateSchedules(
								String.valueOf(loginInfo.getId()),
								date);

						request.setAttribute("thatDateSchedules", thatDateSchedules);

						if (thatDateSchedules == null || thatDateSchedules.size() == 0) {
							request.setAttribute("errMsg2", "予定はありません");
						}
						//index.jspにフォワード
						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
						dispatcher.forward(request, response);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("ErrorServlet");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータ文字コードを指定
		request.setCharacterEncoding("UTF-8");
		try {
			String[] checkList = request.getParameterValues("checked");
			if (checkList != null) {
				ScheduluDao scheduluDao = new ScheduluDao();
				for (String check : checkList) {
					scheduluDao.checked(check);
				}
				response.sendRedirect("IndexServlet");
			} else {
				response.sendRedirect("IndexServlet");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("ErrorServlet");
		}
	}
}
