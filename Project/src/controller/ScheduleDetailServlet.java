package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Category;
import beans.Schedule;
import beans.User;
import dao.CategoryDao;
import dao.ScheduluDao;

/**
 * Servlet implementation class ScheduleDetailServlet
 */
@WebServlet("/ScheduleDetailServlet")
public class ScheduleDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		try {
			User loginInfo = (User) session.getAttribute("loginInfo");

			if (loginInfo == null) {
				response.sendRedirect("LoginServlet");
			} else {

				String scheduleId = request.getParameter("id");

				System.out.println(scheduleId);

				ScheduluDao scheduluDao = new ScheduluDao();

				Schedule scheduleDetail = scheduluDao.scheduleDetail(scheduleId);

				System.out.println(scheduleDetail);

				request.setAttribute("scheduleDetail", scheduleDetail);

				CategoryDao categoryDao = new CategoryDao();
				List<Category> categories = categoryDao.findCategories(String.valueOf(loginInfo.getId()));

				request.setAttribute("categories", categories);

				//スケジュール詳細画面にフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/scheduleDetail.jsp");
				dispatcher.forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("ErrorServlet");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータ文字コードを指定
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			User loginInfo = (User) session.getAttribute("loginInfo");

			String scheduleId = request.getParameter("id");
			String check = request.getParameter("checked");
			String date = request.getParameter("date");
			String time = request.getParameter("time");
			String category = request.getParameter("category");
			String title = request.getParameter("title");
			String memo = request.getParameter("memo");

			if (date.equals("") ||
					time.equals("") ||
					category.equals("") ||
					title.equals("")) {

				request.setAttribute("errMsg1", "詳細・メモ以外の空欄を全て埋めてください。");

				String sId = request.getParameter("id");

				ScheduluDao scheduluDao = new ScheduluDao();

				Schedule scheduleDetail = scheduluDao.scheduleDetail(sId);

				request.setAttribute("scheduleDetail", scheduleDetail);

				CategoryDao categoryDao = new CategoryDao();
				List<Category> categories = categoryDao.findCategories(String.valueOf(loginInfo.getId()));

				request.setAttribute("categories", categories);

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/scheduleDetail.jsp");
				dispatcher.forward(request, response);
				return;
			}

			ScheduluDao scheduluDao = new ScheduluDao();
			scheduluDao.scheduleUpdate(check, date, time, category, title, memo, scheduleId);

			response.sendRedirect("IndexServlet");

		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("ErrorServlet");
		}
	}
}
