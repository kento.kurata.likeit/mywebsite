package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Schedule;
import beans.User;
import dao.ScheduluDao;

/**
 * Servlet implementation class ScheduleDeleteServlet
 */
@WebServlet("/ScheduleDeleteServlet")
public class ScheduleDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			User loginInfo = (User) session.getAttribute("loginInfo");

			if (loginInfo == null) {
				response.sendRedirect("LoginServlet");
			} else {
				String scheduleId = request.getParameter("id");

				System.out.println(scheduleId);

				ScheduluDao scheduluDao = new ScheduluDao();

				Schedule scheduleDetail = scheduluDao.scheduleDetail(scheduleId);

				System.out.println(scheduleDetail);

				request.setAttribute("scheduleDetail", scheduleDetail);

				//スケジュール詳細画面にフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/scheduleDelete.jsp");
				dispatcher.forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("ErrorServlet");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータ文字コードを指定
		request.setCharacterEncoding("UTF-8");
		try {
			String scheduleId = request.getParameter("id");

			System.out.println(scheduleId);

			ScheduluDao scheduluDao = new ScheduluDao();

			scheduluDao.scheduleDelete(scheduleId);

			response.sendRedirect("IndexServlet");
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("ErrorServlet");
		}
	}
}
