package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Category;
import beans.User;
import dao.CategoryDao;
import dao.ScheduluDao;

/**
 * Servlet implementation class AddScheduleServlet
 */
@WebServlet("/AddScheduleServlet")
public class AddScheduleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			User loginInfo = (User) session.getAttribute("loginInfo");

			if (loginInfo == null) {
				response.sendRedirect("LoginServlet");
			} else {

				CategoryDao categoryDao = new CategoryDao();
				List<Category> categories = categoryDao.findCategories(String.valueOf(loginInfo.getId()));

				request.setAttribute("categories", categories);

				//addSchedule.jspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/addSchedule.jsp");
				dispatcher.forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("ErrorServlet");
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータ文字コードを指定
		request.setCharacterEncoding("UTF-8");
		try {
			// リクエストパラメータの入力項目を取得
			String userId = request.getParameter("id");
			String date = request.getParameter("date");
			String time = request.getParameter("time");
			String categoryId = request.getParameter("categoryId");
			String title = request.getParameter("title");
			String memo = request.getParameter("memo");

			if (date.equals("") ||
					time.equals("") ||
					categoryId.equals("") ||
					title.equals("")) {

				request.setAttribute("errMsg1", "詳細・メモ以外の空欄を全て入力してください。");

				HttpSession session1 = request.getSession();
				User loginInfo = (User) session1.getAttribute("loginInfo");

				CategoryDao categoryDao = new CategoryDao();
				List<Category> categories = categoryDao.findCategories(String.valueOf(loginInfo.getId()));

				request.setAttribute("categories", categories);

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/addSchedule.jsp");
				dispatcher.forward(request, response);
				return;
			}

			ScheduluDao scheduluDao = new ScheduluDao();
			scheduluDao.addSchedule(date, time, title, memo, categoryId, userId);

			response.sendRedirect("IndexServlet");

		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("ErrorServlet");
		}
	}
}
