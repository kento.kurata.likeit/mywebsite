package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Category;
import beans.Schedule;
import beans.User;
import dao.CategoryDao;
import dao.ScheduluDao;

/**
 * Servlet implementation class SerchScheduleServlet
 */
@WebServlet("/SerchScheduleServlet")
public class SerchScheduleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		try {
			User loginInfo = (User) session.getAttribute("loginInfo");

			if (loginInfo == null) {
				response.sendRedirect("LoginServlet");
			} else {
				CategoryDao categoryDao = new CategoryDao();
				List<Category> categories = categoryDao.findCategories(String.valueOf(loginInfo.getId()));

				request.setAttribute("categories", categories);

				//scheduleSearch.jspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/scheduleSearch.jsp");
				dispatcher.forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("ErrorServlet");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータ文字コードを指定
		request.setCharacterEncoding("UTF-8");

		try {
			// リクエストパラメータの入力項目を取得
			String userId = request.getParameter("id");
			String check = request.getParameter("checked");
			String scheduleDate = request.getParameter("date");
			String scheduleTime1 = request.getParameter("startTime");
			String scheduleTime2 = request.getParameter("finTime");
			String scheduleTitle = request.getParameter("title");
			String scheduleMemo = request.getParameter("memo");
			String categoryId = request.getParameter("categoryId");

			ScheduluDao scheduluDao = new ScheduluDao();
			List<Schedule> schedules = scheduluDao.scheduleSreach(check, scheduleDate, scheduleTime1, scheduleTime2,
					scheduleTitle, scheduleMemo, categoryId, userId);

			request.setAttribute("schedules", schedules);

			System.out.println(schedules);

			if (schedules == null || schedules.size() == 0) {
				request.setAttribute("errMsg1", "該当する予定はありません。");

				CategoryDao categoryDao = new CategoryDao();
				List<Category> categories = categoryDao.findCategories(userId);

				request.setAttribute("categories", categories);

				//scheduleSreach.jspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/scheduleSearch.jsp");
				dispatcher.forward(request, response);
			} else {
				//scheduleSreach.jspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/scheduleSearch.jsp");
				dispatcher.forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("ErrorServlet");
		}
	}
}
