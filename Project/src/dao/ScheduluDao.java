package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.Schedule;

public class ScheduluDao {
	//スケジュール追加メソッド
	public void addSchedule(String scheduleDate, String scheduleTime, String scheduleTitle, String scheduleMemo,
			String categoryId, String userId) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "insert into t_schedule (schedule_date, schedule_time, schedule_title, schedule_memo, category_id, user_id, checked) values (?, ?, ?, ?, ?, ?, '0')";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, scheduleDate);
			pStmt.setString(2, scheduleTime);
			pStmt.setString(3, scheduleTitle);
			pStmt.setString(4, scheduleMemo);
			pStmt.setString(5, categoryId);
			pStmt.setString(6, userId);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//今日の検索メソッド
	public List<Schedule> todaySchedules(String userId) {
		Connection conn = null;
		List<Schedule> todaySchedules = new ArrayList<Schedule>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			// 確認済みのSQL
			String sql = "select s.schedule_id, s.schedule_date, s.schedule_time, s.schedule_title, c.category_name, s.checked from t_schedule s join r_category c on s.category_id = c.id where s.user_id = ? and s.schedule_date = DATE(NOW()) order by s.schedule_time asc";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, userId);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int scheduleId = rs.getInt("schedule_id");
				Date scheduleDate = rs.getDate("schedule_date");
				Time scheduleTime = rs.getTime("schedule_time");
				String scheduleTitle = rs.getString("schedule_title");
				String categoryName = rs.getString("category_name");
				int checked = rs.getInt("checked");
				Schedule schedule = new Schedule(scheduleId, scheduleDate, scheduleTime, scheduleTitle, categoryName,
						checked);

				todaySchedules.add(schedule);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return todaySchedules;
	}

	//スケジュール詳細メソッド
	public Schedule scheduleDetail(String scheduleId) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "SELECT t.schedule_id, t.schedule_date, t.schedule_time, t.schedule_title, t.schedule_memo, t.category_id, t.user_id, c.category_name, t.checked FROM t_schedule t join r_category c on t.category_id =  c.id WHERE t.schedule_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, scheduleId);

			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			int scheduleI = rs.getInt("schedule_id");
			Date scheduleDate = rs.getDate("schedule_date");
			Time scheduleTime = rs.getTime("schedule_time");
			String scheduleTitle = rs.getString("schedule_title");
			String scheduleMemo = rs.getString("schedule_memo");
			int categoryId = rs.getInt("category_id");
			int userId = rs.getInt("user_id");
			String categoryName = rs.getString("category_name");
			int check = rs.getInt("checked");

			Schedule schedule = new Schedule(scheduleI, scheduleDate, scheduleTime, scheduleTitle, scheduleMemo,
					categoryId, userId, categoryName, check);

			return schedule;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//更新メソッド
	public void scheduleUpdate(String checked, String date, String time, String category, String title, String memo,
			String scheduleId) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "update t_schedule set";

			if (!checked.equals("")) {
				sql += "  checked ='" + checked + "'";
			}

			if (!date.equals("")) {
				sql += " , schedule_date ='" + date + "'";
			}

			if (!time.equals("")) {
				sql += " , schedule_time = '" + time + "'";
			}
			if (!category.equals("")) {
				sql += " , category_id = '" + category + "'";
			}
			if (!title.equals("")) {
				sql += " , schedule_title = '" + title + "'";
			}
			if (!memo.equals("")) {
				sql += " , schedule_memo = '" + memo + "'";
			}
			if (memo.equals("")) {
				sql += " , schedule_memo = '" + "" + "'";
			}
			if (!scheduleId.equals("")) {
				sql += " where schedule_id = '" + scheduleId + "'";
			}

			Statement stmt = conn.createStatement();

			stmt.executeUpdate(sql);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//スケジュール削除メソッド
	public void scheduleDelete(String scheduleId) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "delete from t_schedule where schedule_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, scheduleId);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//スケジュール検索メソッド
	public List<Schedule> scheduleSreach(String checked, String scheduleDate, String scheduleTime1,
			String scheduleTime2,
			String scheduleTitle, String scheduleMemo, String categoryId, String userId) {
		Connection conn = null;
		List<Schedule> schedules = new ArrayList<Schedule>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			// 確認済みのSQL
			String sql = "select s.schedule_id, s.checked, s.schedule_date, s.schedule_time, s.schedule_title, s.schedule_memo, c.category_name from t_schedule s join r_category c on s.category_id = c.id";

			if (!userId.equals("")) {
				sql += " where s.user_id ='" + userId + "'";
			}
			if (!checked.equals("")) {
				sql += " and s.checked ='" + checked + "'";
			}
			if (checked.equals("")) {
				sql += " and s.checked is not null";
			}
			if (!scheduleDate.equals("")) {
				sql += " and s.schedule_date ='" + scheduleDate + "'";
			}
			if (!scheduleTime1.equals("")) {
				sql += " and s.schedule_time >='" + scheduleTime1 + "'";
			}
			if (!scheduleTime2.equals("")) {
				sql += " and s.schedule_time <='" + scheduleTime2 + "'";
			}
			if (!scheduleTitle.equals("")) {
				sql += " and s.schedule_title like '%" + scheduleTitle + "%'";
			}
			if (!scheduleMemo.equals("")) {
				sql += " and s.schedule_memo like '%" + scheduleMemo + "%'";
			}
			if (!categoryId.equals("")) {
				sql += " and s.category_id ='" + categoryId + "'";
			}
			if (!userId.equals("")) {
				sql += " order by s.schedule_date desc, s.schedule_time desc";
			}

			PreparedStatement pStmt = conn.prepareStatement(sql);

			ResultSet rs = pStmt.executeQuery();
			System.out.println(sql);
			while (rs.next()) {
				int sId = rs.getInt("schedule_id");
				Date sDate = rs.getDate("schedule_date");
				Time sTime = rs.getTime("schedule_time");
				String sTitle = rs.getString("schedule_title");
				String sMemo = rs.getString("schedule_memo");
				String cName = rs.getString("category_name");
				int check = rs.getInt("checked");

				Schedule schedule = new Schedule(sId, sDate, sTime, sTitle, sMemo, cName, check);

				schedules.add(schedule);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return schedules;
	}

	//指定日スケジュール検索メソッド
	public List<Schedule> thatDateSchedules(String userId, String date) {
		Connection conn = null;
		List<Schedule> thatDateSchedules = new ArrayList<Schedule>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			// 確認済みのSQL
			String sql = "select s.schedule_id, s.schedule_date, s.schedule_time, s.schedule_title, c.category_name, s.checked from t_schedule s join r_category c on s.category_id = c.id where s.user_id = ? and s.schedule_date = ? order by s.schedule_time asc";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, userId);
			pStmt.setString(2, date);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int scheduleId = rs.getInt("schedule_id");
				Date scheduleDate = rs.getDate("schedule_date");
				Time scheduleTime = rs.getTime("schedule_time");
				String scheduleTitle = rs.getString("schedule_title");
				String categoryName = rs.getString("category_name");
				int checked = rs.getInt("checked");

				Schedule schedule = new Schedule(scheduleId, scheduleDate, scheduleTime, scheduleTitle, categoryName,
						checked);

				thatDateSchedules.add(schedule);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return thatDateSchedules;
	}

	//チェックメソッド
	public void checked(String scheduleId) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "update t_schedule set checked ='1' where schedule_id =?";

			PreparedStatement pSmt = conn.prepareStatement(sql);

			pSmt.setString(1, scheduleId);

			pSmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
