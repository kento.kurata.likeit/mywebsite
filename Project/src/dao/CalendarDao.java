package dao;

public class CalendarDao {

	public int getDayOfWeek(int year,int month, int day) {
		int dayOfWeek;
		if(month<3) {
			year--;
			month+=12;
		}
		dayOfWeek = (year + year/4 - year/100 + year/400 + (13*month+8)/5 + day)%7;
		return dayOfWeek;
	}

	public int isLeepYear(int year) {
		int leepYear = 0;
		if(year%4==0 && year%100!=0 || year%400==0) {
			leepYear = 1;
		}
		return leepYear;

	}
}