package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import beans.User;

public class UserDao {

	//ログイン(詳細)メソッド
	public User findByLoginId(String loginId, String password) {
		Connection conn = DBManager.getConnection();
		try {

			//SQL
			String sql = "SELECT id, login_id, name FROM t_user WHERE login_id = ? and password = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int userId = rs.getInt("id");
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");

			return new User(userId, loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//新規登録メソッド
	public void userCreate(String name, String loginId, String password) {
		Connection conn = null;
		try {
			//データベース接続
			conn = DBManager.getConnection();
			//SQL
			String sql = "INSERT INTO `t_user`(name, login_id, password) VALUES (?, ?, ?)";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, loginId);
			pStmt.setString(3, password);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//loginIdの重複確認メソッド
	public User findLoginId(String loginId) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT login_id FROM t_user WHERE login_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String loginIdData = rs.getString("login_id");

			return new User(loginIdData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//参照メソッド
	public User ucerDetail(String id) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();
			String sql = "SELECT id,login_id, name FROM t_user WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, id);

			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int id1 = rs.getInt("id");
			String name = rs.getString("name");
			String loginId = rs.getString("login_id");
			User user = new User(id1, name, loginId);

			return user;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//更新メソッド
	public void userUpdate(String name, String password, String id) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "update t_user set";

			if (!name.equals("")) {
				sql += "  name ='" + name + "'";
			}

			if (!password.equals("")) {
				sql += " , password = '" + password + "'";
			}

			if (!id.equals("")) {
				sql += " where id = '" + id + "'";
			}

			Statement stmt = conn.createStatement();

			stmt.executeUpdate(sql);

			System.out.println(sql);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	//暗号化
		public String secret(String password) {
			String source = password;
			//ハッシュ生成前にバイト配列に置き換える際のCharset
			Charset charset = StandardCharsets.UTF_8;
			//ハッシュアルゴリズム
			String algorithm = "MD5";

			//ハッシュ生成処理
			byte[] bytes = null;
			try {
				bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			} catch (NoSuchAlgorithmException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
			String result = DatatypeConverter.printHexBinary(bytes);
			//標準出力
			System.out.println(result);
			return result;
		}
}
