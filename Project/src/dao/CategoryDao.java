package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.Category;

public class CategoryDao {
	//カテゴリー表示メソッド
	public List<Category> findCategories(String createUserId) {
		Connection conn = null;
		List<Category> categories = new ArrayList<Category>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			// 確認済みのSQL
			String sql = "select id, category_name, create_user_id from r_category where create_user_id is null or create_user_id =?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, createUserId);

			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String categoryName = rs.getString("category_name");
				int createUserId1 = rs.getInt("create_user_id");

				Category category = new Category(id, categoryName, createUserId1);

				categories.add(category);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return categories;
	}

	//追加メソッド
	public void addCategory(String categoryName, String createUserId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			//SQL
			String sql = "insert into r_category (category_name, create_user_id) values (?, ?)";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, categoryName);
			pStmt.setString(2, createUserId);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//カテゴリー選択メソッド
	public Category categoryDetail(String id) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();
			String sql = "SELECT * FROM r_category WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, id);

			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int id1 = rs.getInt("id");
			String categoryName = rs.getString("category_name");
			int createUserId = rs.getInt("create_user_id");

			Category category = new Category(id1, categoryName, createUserId);

			return category;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//カテゴリー削除メソッド
	public void categoryDelete(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			//SQL
			String sql = "delete from r_category where id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	//カテゴリー重複確認
	public Category findCateName(String categoryName) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT category_name FROM r_category WHERE category_name = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, categoryName);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String cateName = rs.getString("Category_name");

			return new Category(cateName);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
}
